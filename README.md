# A WordPress dev template with PhpMyAdmin on Gitpod

This is a (real) [Wordpress](wordpress) template configured for ephemeral development environments on [Gitpod](https://www.gitpod.io/).

It installs the latest WordPress version and an instance of [phpMyAdmin](https://www.phpmyadmin.net/) to navigate through the database if needed.

# How to use it ?

## Prepare

Fork this project or add all the files to an existing empty repository that you have already created.

Adapt the config as you wish.

You should activate the integration of GitPod in [GitLab](https://docs.gitlab.com/ee/integration/gitpod.html#enable-gitpod-in-your-user-settings), [GitHub](https://github.com/apps/gitpod-io/installations/new) or [BitBucket](https://www.gitpod.io/docs/bitbucket-integration).

You can also use the [GitPod browser extension](https://www.gitpod.io/docs/browser-extension).

## Use

In your repository page, you have now a button to open the repository in GitPod.

Click on it and wait for the Container to boot.

You can then do some work and commit it to your repository

## More infos

WordPress is installed and fully accessible in a new `wpdata` directory at the root of your project and exposed on port 8000.

Note that regarding .gitignore, only your work in `wpdata/wp-content` is versioned.

PhpMyAdmin is exposed on port 8001.